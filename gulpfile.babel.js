import gulp from 'gulp';
import {production, developers, config} from './config';
require('require-dir')('./node_modules/gromov-core-templates/tasks');

// Сборка проекта
gulp.task('build',
	gulp.series('clean', gulp.parallel('png-sprite', 'svg-sprite', 'img', 'fonts', 'js'), 'style')
);

// Запуск
gulp.task('null', function(callback) {
	callback()
});


//Запуск
gulp.task('default',
    gulp.series('build', developers ? gulp.parallel('server', 'watch') : 'null')
);