import $ from 'jquery';
import device from 'current-device'

// ===========================================
//
//		Браузеры
//
// ===========================================


if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
    $('html').addClass('firefox')
}

if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1){
    $('html').addClass('safari')
}

if(navigator.userAgent.toLowerCase().indexOf('yabrowser/') > -1){
    $('html').addClass('yandex')
}

if((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){
    $('html').addClass('opera')
}


// ===========================================
//
//		OS
//
// ===========================================

if (device.desktop()) {

	if (navigator.appVersion.indexOf("Win") != -1){
		$('html').addClass('windows')
	}

	if (navigator.appVersion.indexOf("Mac") != -1){
		$('html').addClass('macos')
	}

	if (navigator.appVersion.indexOf("X11") != -1){
		$('html').addClass('unix')
	}

	if (navigator.appVersion.indexOf("Linux") != -1){
		$('html').addClass('linux')
	}

	if(/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)){
	    $('html').addClass('chrome')
	}

	if (document.all) {
	    $('html').addClass('ie')
	}

	if (document.all && !window.atob) {
		$('html').addClass('ie9')
	}

	if (Function('/*@cc_on return document.documentMode===10@*/')()){
	    $('html').addClass('ie10')
	}

	if (!!window.MSInputMethodContext && !!document.documentMode){
	    $('html').addClass('ie ie11')
	}

	if (/Edge\/\d./i.test(navigator.userAgent)) {
	    $('html').addClass('edge')
	}
}